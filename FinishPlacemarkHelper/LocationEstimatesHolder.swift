//
//  LocationEstimatesHolder.swift
//  FinishPlacemarkHelper
//
//  Created by Rizabek on 16.07.2020.
//  Copyright © 2020 Yandex, LLC. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationEstimatesHolder {
    var bestLocationEstimate: SceneLocationEstimate? { get }
    var estimates: [SceneLocationEstimate] { get }

    func add(_ locationEstimate: SceneLocationEstimate)
    func filter(_ isIncluded: (SceneLocationEstimate) -> Bool)
}
