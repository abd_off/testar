//
//  AdvancedLocationEstimatesHolder.swift
//  FinishPlacemarkHelper
//
//  Created by Rizabek on 16.07.2020.
//  Copyright © 2020 Yandex, LLC. All rights reserved.
//

import Foundation

class AdvancedLocationEstimatesHolder: BasicLocationEstimatesHolder {

    override func add(_ locationEstimate: SceneLocationEstimate) {
        for estimate in estimates {
            guard !estimate.canReplace(locationEstimate) else { return }
        }
        super.add(locationEstimate)
        filter { !locationEstimate.canReplace($0) }
    }
}
