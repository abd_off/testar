//
//  ViewController.swift
//  FinishPlacemarkHelper
//
//  Created by Dmitry Trimonov on 16/04/2018.
//  Copyright © 2018 Yandex, LLC. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import CoreLocation
import MapKit

class ViewController: UIViewController, ARSCNViewDelegate {
  
  @IBOutlet var sceneView: ARSCNView!
  var engine: ARKitCoreLocationEngine!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Set the view's delegate
    sceneView.delegate = self
    
    // Show statistics such as fps and timing information
    sceneView.showsStatistics = true
    
    // Create a new scene
    let scene = SCNScene()
    
    // Set the scene to the view
    sceneView.scene = scene
    sceneView.autoenablesDefaultLighting = true
    
    engine = ARKitCoreLocationEngineImpl(
      view: sceneView,
      locationManager: NativeLocationManager.sharedInstance,
      locationEstimatesHolder: AdvancedLocationEstimatesHolder()
    )
    
    routeFinishHint = UIView()
    routeFinishHint.isHidden = true
    routeFinishHint.frame = CGRect(x: 0.0, y: 0.0, width: 50, height: 50)
    routeFinishHint.layer.cornerRadius = 25.0
    routeFinishHint.backgroundColor = UIColor.red
    
    view.addSubview(routeFinishHint)
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    // Create a session configuration
    let configuration = ARWorldTrackingConfiguration()
    configuration.worldAlignment = .gravityAndHeading
    
    // Run the view's session
    sceneView.session.run(configuration)
    
  }
  
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    routeFinishNode = createSphereNode(withRadius: 1.5, color: UIColor.green)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
    // Pause the view's session
    sceneView.session.pause()
  }
  
  func renderer(_ renderer: SCNSceneRenderer, didRenderScene scene: SCNScene, atTime time: TimeInterval) {
    
    guard let routeFinishNode = routeFinishNode else {
      return
    }
    
    routeFinishNode.forEach { (node) in
      guard let parent = node.parent else { return }
      guard let pointOfView = renderer.pointOfView else { return }
      
      let bounds = UIScreen.main.bounds
      
      let positionInWorld = node.worldPosition
      let positionInPOV = parent.convertPosition(node.position, to: pointOfView)
      let projection = sceneView.projectPoint(positionInWorld)
      let projectionPoint = CGPoint(x: CGFloat(projection.x), y: CGFloat(projection.y))
      
      let annotationPositionInRFN = SCNVector3Make(0.0, 1.0, 0.0) // in Route finish node coord. system
      let annotationPositionInWorld = node.convertPosition(annotationPositionInRFN, to: nil)
      let annotationProjection = sceneView.projectPoint(annotationPositionInWorld)
      let annotationProjectionPoint = CGPoint(x: CGFloat(annotationProjection.x), y: CGFloat(annotationProjection.y))
      let rotationAngle = Vector2.y.angle(with: (Vector2(annotationProjectionPoint) - Vector2(projectionPoint)))
      
      let screenMidToProjectionLine = CGLine(point1: bounds.mid, point2: projectionPoint)
      let intersection = screenMidToProjectionLine.intersection(withRect: bounds)
      
      let povWorldPosition: Vector3 = Vector3(pointOfView.worldPosition)
      let routeFinishWorldPosition: Vector3 = Vector3(positionInWorld)
      let distanceToFinishNode = (povWorldPosition - routeFinishWorldPosition).length
      
      DispatchQueue.main.async { [weak self] in
        guard let slf = self else { return }
        guard let routeFinishHint = slf.routeFinishHint else { return }
        
        let placemarkSize = ViewController.finishPlacemarkSize(
          forDistance: CGFloat(distanceToFinishNode),
          closeDistance: 10.0,
          farDistance: 25.0,
          closeDistanceSize: 100.0,
          farDistanceSize: 50.0
        )
        
        let distance = floor(distanceToFinishNode)
        
        let point: CGPoint = intersection ?? projectionPoint
        let isInFront = positionInPOV.z < 0
        let isProjectionInScreenBounds: Bool = intersection == nil
        
        
        if isInFront {
          routeFinishHint.center = point
        } else {
          if isProjectionInScreenBounds {
            routeFinishHint.center = CGPoint(
              x: reflect(point.x, of: bounds.mid.x),
              y: bounds.height
            )
          } else {
            routeFinishHint.center = CGPoint(
              x: reflect(point.x, of: bounds.mid.x),
              y: reflect(point.y, of: bounds.mid.y)
            )
          }
        }

      }
    }
    
  }

  
  func createSphereNode(withRadius radius: CGFloat, color: UIColor) -> [SCNNode] {
    
    let placeLocation1 = CLLocationCoordinate2D(latitude: 42.96673479941999, longitude: 47.501983444799194)
    
    let placeLocation2 = CLLocationCoordinate2D(latitude: 42.96521627589293, longitude: 47.500548362731934)
    
    let placeLocation3 = CLLocationCoordinate2D(latitude: 42.96626174698166, longitude: 47.50189689259035)
    
    let placeLocation4 = CLLocationCoordinate2D(latitude: 42.969447827231264, longitude: 47.494025230407715)
    
    let placeLocation5 = CLLocationCoordinate2D(latitude: 42.0623194, longitude: 48.2902375)
    
    let placeLocation6 = CLLocationCoordinate2D(latitude: 42.93489650175477, longitude: 47.535715023336586)
    
    let placeLocation7 = CLLocationCoordinate2D(latitude: 42.93524211563893, longitude: 47.538847843465994)
    
    let placeLocation8 = CLLocationCoordinate2D(latitude: 42.966553210256045, longitude: 47.50142616490824)
    
    let placeLocation = [placeLocation1, placeLocation2, placeLocation3, placeLocation4, placeLocation5, placeLocation6, placeLocation7, placeLocation8]
    
    
    var spheresArray : [SCNNode] = []
    let sphereGeometry = SCNSphere(radius: radius)
    let sphereMaterial = SCNMaterial()
    sphereMaterial.diffuse.contents = color
    sphereGeometry.materials = [sphereMaterial]
    
    
    guard let userLocation = self.engine.userLocationEstimate()?.location.coordinate else { return  [SCNNode]()}
    let currentLocation = CLLocation(latitude: userLocation.latitude, longitude: userLocation.longitude)
    
    placeLocation.forEach { (location) in
      let locPlace = CLLocation(latitude: location.latitude, longitude: location.longitude)
      print(locPlace)
      let placeLocation = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
      print(placeLocation)
      let distanceInMeters = currentLocation.distance(from: locPlace)
      print(distanceInMeters)
      
      if distanceInMeters < 501 {
        print(distanceInMeters)
        
        let request = MKDirections.Request.init()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: userLocation))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: placeLocation))
        request.transportType = .walking
        let directions = MKDirections(request: request)
        let spheres = SCNNode(geometry: sphereGeometry)
        
        directions.calculate { [unowned self] (response, error) -> Void in
          guard let response = response else { return }
          let route = response.routes
          
          route.forEach { (route) in
            let routePointsCount = route.polyline.pointCount
            let routePoints = route.polyline.points()
            var geoRoute: [CLLocationCoordinate2D] = []
            for pointIndex in 0..<routePointsCount {
              let point: MKMapPoint = routePoints[pointIndex]
              geoRoute.append(MKCoordinateForMapPoint(point))
            }
            
            let route = geoRoute
              .map { self.engine.convert(coordinate: $0) }
              .compactMap { $0 }
              .map { CGPoint(position: $0) }
            
            guard route.count == geoRoute.count else {
              return
            }
            
            let routeFinishPoint = route
            
            routeFinishPoint.forEach { (point) in
              
              spheres.position = point.positionIn3D
              self.sceneView.scene.rootNode.addChildNode(spheres)
              spheresArray.append(spheres)
              
            }
          }
          
        }
        
      }
    }
    
    return spheresArray
  }
  
  private var routeFinishNode: [SCNNode]!
  private var routeFinishHint: UIView!
  
  
  
  static func distanceText(forString string: String) -> NSAttributedString {
    return NSMutableAttributedString(string: string, attributes: [
      .strokeColor : UIColor.black,
      .foregroundColor : UIColor.white,
      .strokeWidth : -1.0,
      .font : UIFont.boldSystemFont(ofSize: 32.0)
    ])
  }
  
  /// RouteFinishPlacemark size driven by design requirements
  ///
  /// - Parameters:
  ///   - distance: distance to route finish
  static func finishPlacemarkSize(forDistance distance: CGFloat, closeDistance: CGFloat, farDistance: CGFloat,
                                  closeDistanceSize: CGFloat, farDistanceSize: CGFloat) -> CGFloat
  {
    guard closeDistance >= 0 else { assert(false); return 0.0 }
    guard closeDistance >= 0, farDistance >= 0, closeDistance <= farDistance else { assert(false); return 0.0 }
    
    if distance > farDistance {
      return farDistanceSize
    } else if distance < closeDistance{
      return closeDistanceSize
    } else {
      let delta = farDistanceSize - closeDistanceSize
      let percent: CGFloat = ((distance - closeDistance) / (farDistance - closeDistance))
      let size = closeDistanceSize + delta * percent
      return size
    }
  }
  
  func findProjection(ofNode node: SCNNode, inSceneOfView scnView: SCNView) -> CGPoint {
    let nodeWorldPosition = node.worldPosition
    let projection = scnView.projectPoint(nodeWorldPosition)
    return CGPoint(x: CGFloat(projection.x), y: CGFloat(projection.y))
  }
  
  func isNodeInFrontOfCamera(_ node: SCNNode, scnView: SCNView) -> Bool {
    guard let pointOfView = scnView.pointOfView else { return false }
    guard let parent = node.parent else { return false }
    let positionInPOV = parent.convertPosition(node.position, to: pointOfView)
    return positionInPOV.z < 0
  }
}
